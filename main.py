import pandas as pd
import re
import os
import nltk
import numpy as np
from bs4 import BeautifulSoup
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.ensemble import RandomForestClassifier

def review_to_words( raw_review ):

    # 1. Remove HTML
    review_text = BeautifulSoup(raw_review).get_text() 
 
    # 2. Remove non-letters        
    letters_only = re.sub("[^a-zA-Z]", " ", review_text) 
   
    # 3. Convert to lower case, split into individual words
    words = letters_only.lower().split()                             
  
    # 4. In Python, searching a set is much faster than searching a list, so convert the stop words to a set
    stops = set(stopwords.words("english"))                  
    
    # 5. Remove stop words
    meaningful_words = [w for w in words if not w in stops]   
    
    # 6. Join the words back into one string separated by space, and return the result.
    return( " ".join( meaningful_words ))

train = pd.read_csv("labeledTrainData.tsv", header=0, delimiter="\t", quoting=3)
num_reviews = train["review"].size
clean_train_reviews = []
vectorizer = CountVectorizer(analyzer = "word", tokenizer = None, preprocessor = None, stop_words = None, max_features = 5000) 

print("Cleaning and parsing the training set movie reviews...\n")

for i in list(range(0, num_reviews)):
    if((i+1)%1000 == 0 ):
        print("Review %d of %d\n" % ( i+1, num_reviews ) )                                                                   
    clean_train_reviews.append( review_to_words( train["review"][i] ))
    # file = open(os.path.join('A:/Developer/Case Study/bag_of_words/cleaned-review/', 'review-%d.txt' % i),"w") 
    # file.write(clean_train_reviews[i]) 

train_data_features = [] 
train_data_features = vectorizer.fit_transform(clean_train_reviews)
train_data_features = train_data_features.toarray()
file = open(os.path.join('A:/Developer/Case Study/bag_of_words/', 'tdm.txt'),"w")
for item in train_data_features:
    file.writelines("%s" % item) 

vocab = vectorizer.get_feature_names()
dist = np.sum(train_data_features, axis=0)
for tag, count in zip(vocab, dist):
    print (count, tag)

#-------------------TRAINING-----------------------#
print("Training the random forest...")
forest = RandomForestClassifier(n_estimators = 100) 
forest = forest.fit(train_data_features, train["sentiment"] )
print(forest)

#-------------------TESTING------------------------#
test = pd.read_csv("testData.tsv", header=0, delimiter="\t", quoting=3)
print(test.shape)
clean_test_reviews = []

print("Cleaning and parsing the test set movie reviews...\n")
for i in list(range(0,num_reviews)):
    if((i+1) % 1000 == 0):
        print ("Review %d of %d\n" % (i+1, num_reviews))
    clean_review = review_to_words( test["review"][i] )
    clean_test_reviews.append(clean_review)

test_data_features = []
test_data_features = vectorizer.transform(clean_test_reviews)
test_data_features = test_data_features.toarray()

print("Testing the random forest model...")
result = forest.predict(test_data_features)

output = pd.DataFrame( data={"id":test["id"], "review":test["review"], "sentiment":result} )

output.to_csv( "Bag_of_Words_model.csv", index=False, quoting=3, escapechar='\\' )
print("Work is done")




# print(train.shape)
# print(train.columns.values)
# print(train["sentiment"][7])
# print(train["review"][7])

# example1 = BeautifulSoup(train["review"][0])
# letters_only = re.sub("[^a-zA-Z]", " ", example1.get_text())
# print (letters_only)
# print("--------------")
# lower_case = letters_only.lower()
# words = lower_case.split()
# print(words)
# print("---------------")
# wordsAfter = [w for w in words if not w in stopwords.words("english")]
# print(wordsAfter)